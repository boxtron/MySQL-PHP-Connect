Tutorial MySQL/PHP Connect
--------------

Dies ist eine Anleitung für das Erstellen einer einfachen Webapplikation mit Datenbankanbindung. Es sollen Daten aus einer Datenbank wahlweise als Tabelle oder als einzelner Datensatz in einem Browserfenster (html-Dokument) dargestellt werden. Dabei soll es möglich sein, aus der Tabelle einen Datensatz auszuwählen, diesen dann einzeln und detailliert auf einer neuen Seite zu präsentieren. Die „Applikation“ besteht am Anfang nur aus einer Tabelle, aber an ihr können Sie die Einzelschritte einfach nachvollziehen. Als Beispiel dient eine Internet-Auktion. Für die generelle Anbindung PHP und MySQL reicht das aus. Sie können das Beispiel nach eigenen Wünschen weiter ausbauen (Datenerfassung, weitere Tabellen usw.).  


Schritt 1: Datenbasis & Benutzer erstellen (Vorbereitung Umgebung)
-----

Datenbankname: "auction"  
Tabelle: "angebot"  

SQL-Befehl, anlegen der Tabelle "angebot"  
```sql
CREATE TABLE angebot( angebot_id INT NOT NULL AUTO_INCREMENT,  
 angebot_titel VARCHAR(50) NOT NULL,  
 angebot_beschreibung TEXT NOT NULL,  
 preis_min DECIMAL(10,2) NOT NULL,  
 time_end DATETIME NOT NULL,  
 time_start DATETIME NOT NULL,  
 PRIMARY KEY (angebot_id));
```

SQL-Befehl, einfügen der Datensätze  
```sql
INSERT INTO `angebot` (`angebot_id`, `angebot_titel`, `angebot_beschreibung`, `preis_min`, `time_end`, `time_start`)  
VALUES ('1', 'Kanu (Dagger)', 'Grosse Beschreibung', '990', '2018-06-30 00:00:00', '2018-06-21 00:00:00');  
INSERT INTO `angebot` (`angebot_id`, `angebot_titel`, `angebot_beschreibung`, `preis_min`, `time_end`, `time_start`)  
VALUES ('2', 'Mountainbike', 'Grosse Beschreibung', '50', '2018-06-29 00:00:00', '2018-06-21 00:00:00');  
INSERT INTO `angebot` (`angebot_id`, `angebot_titel`, `angebot_beschreibung`, `preis_min`, `time_end`, `time_start`)  
VALUES ('3', 'Digi-Camera Canon', 'Grosse Beschreibung', '800', '2018-06-28 00:00:00', '2018-06-21 00:00:00');  
INSERT INTO `angebot` (`angebot_id`, `angebot_titel`, `angebot_beschreibung`, `preis_min`, `time_end`, `time_start`)  
VALUES ('4', 'Opel Corsa', 'Grosse Beschreibung', '3750', '2018-06-27 00:00:00', '2018-06-21 00:00:00');  
INSERT INTO `angebot` (`angebot_id`, `angebot_titel`, `angebot_beschreibung`, `preis_min`, `time_end`, `time_start`)  
VALUES ('5', 'Segelboot', 'Grosse Beschreibung', '4500', '2018-06-26 00:00:00', '2018-06-21 00:00:00');
```

Erstellen Sie zu Beginn die Datenbank mit Hilfe eines MySQL-Frontends (z. B. PHPMy-Admin). Um die Berechtigungen für einen Benutzer einzurichten, führen Sie das folgende SQL-Statement aus. Kontrollieren Sie anschliessend die Einträge in den Tabellen der Datenbank „mysql“!  
  
Berechtigungen: php_user mit SELECT, INSERT, UPDATE, DELETE auf auction.*

```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON auction.*  
TO auction_user@localhost IDENTIFIED BY '12345';
```
  
  
Schritt 2: Verbindungsaufbau (Grundlagen)
----

Sie haben nun nach der Präsentation gelernt, dass es zwei unterschiedliche Möglichkeiten für eine Verbindung gibt. Nun schauen Sie in praktischen Beispielen diese Möglichkeiten mit MySQLi und PDO (PHP Data Object) an.  
  
**Nachfolgend das erste Beispiel:**  
Anleitung: Speichern Sie diese Datei mit dem Namen *connent.php* in das Verzeichnis c:\xampp\htdocs (oder in das dafür erstellte Verzeichnis).  
```php
<?php
# Achtung! Ergänzen

$db = new mysqli($host, $user, $pass, $db) or die("Unable to connect");

echo "Great work";

?>
```
Link zu PHP-Doku: [mysqli] (http://php.net/manual/de/function.mysqli-connect.php)  

**Zweites Beispiel:**  
Anleitung: Speichern Sie diese Datei mit dem Namen *connentProc.php* in das Verzeichnis c:\xampp\htdocs (oder in das dafür erstellte Verzeichnis).  
```php
<?php
# Achtung! Ergänzen

$conn = mysqli_connect($host, $user, $pass, $db);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

echo "Great work";
?>
```

**Drittes Beispiel:**  
Anleitung: Speichern Sie diese Datei mit dem Namen *connentPDO.php* in das Verzeichnis c:\xampp\htdocs (oder in das dafür erstellte Verzeichnis).  

```php
<?php
# Achtung! Ergänzen

try {
   $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
      
   if($conn) {
    echo "Connected to the <strong>$db</strong> database successfully";    
   }
}

catch(PDOExeption $e) {
   echo "Connection failed: " . $e->getMessage();
}
?>
```
Link zu PHP-Doku: [PDO] (http://php.net/manual/de/pdo.construct.php)


Schritt 3: Abfragen und Resultate (Plus)
----

Sie haben nun gelernt, wie eine Verbindung zwischen PHP und MySQL möglich ist. Nun möchten Sie natürlich die eingegebenen Daten auch anzeigen lassen. Folgend werden die dafür notwendigen Befehle erläutert.  
  
**Select**    
```php
mysqli_select_db($conn, $db);
```
Die Funktion mysql_select_db() entspricht dem SQL-Befehl use und wählt eine Datenbank des verbundenen Servers aus.

**Query**  
```php
$result = mysqli_query($conn, "SELECT angebot_id, angebot_titel, preis_min, time_end FROM angebot ORDER BY time_end ASC");
```
Der Funktion mysql_query() wird ein SQL-Statement übergeben. Das Resultat steht dann in einer Variablen (hier $result) zur Verfügung.

**Resultat ausgeben**  
Das Resultat der Funktion mysqli_query() kann mit einem zweidimensionalen Array verglichen werden. Damit auf die Inhalte zugegriffen werden kann, muss zuerst eine einzelne Zeile in ein normales (eindimensionales) Array abgefüllt werden. Dies geschieht mit der Funktion mysql_fetch_array().

![Bild 1](./pictures/PHP_Array_fetch.PNG "Darstellung Array")


```php
$zeile = mysqli_fetch_array($result);
```
  
Mit "print" kann jetzt ein beliebiger Wert des Datensatzes ausgegeben werden.  
```php
print "$zeile[angebot_titel]";
```



**Beispiel-Code (Komplett)**  
```php
<?php
$host = '';
$user = '';
$pass = '';
$db = '';

$conn = new mysqli($host, $user, $pass, $db) or die("Unable to connect");
echo "Great work<br/><br/>";

mysqli_select_db($conn, $db);

$result = mysqli_query($conn, "SELECT angebot_id, angebot_titel, preis_min, time_end FROM angebot ORDER BY time_end ASC");

$zeile = mysqli_fetch_array($result);

print "$zeile[angebot_titel]";
?>
```

**Zusatzaufgabe:**  
Wie lesen Sie nun weitere Daten aus der Tabelle "auction" und stellen diese dar? Wie können Sie die "print" Anweisung weiter ausbauen, um alle Datensätze anzeigen zu lassen?  
(Tipp: "while")

Lösung:  
```php
# $zeile = mysqli_fetch_array($result);
# print "$zeile[angebot_titel]";

while($zeile = mysqli_fetch_array($result)){  
    print "$zeile[angebot_titel], Preis: CHF $zeile[preis_min], $zeile[time_end]<br>\n";
}
```

Zusammenfassung
----
Sie haben nun die verschiedenen Möglichkeiten der Verbindung zwischen PHP und SQL angeschaut. Es liegt nun an Ihnen, welche Sie nun in den jeweiligen Programmierungen verwenden möchten.  

**Tabelle (Übersicht)**

| Vergleich                         | PDO                     | MySQLi           |
|-----------------------------------|-------------------------|------------------|
| Datenbank Support                 | 12 verschiedene Treiber | MySQL only       |
| API                               | OOP                     | OOP + Prozedural |
| Connection                        | Einfach                 | Einfach          |
| Named parameters                  | Ja                      | Nein             |
| Object mapping                    | Ja                      | Ja               |
| Prepared Statements (client side) | Ja                      | Nein             |
| Performance                       | Schnell                 | Schnell          |
| Stored procedures                 | Ja                      | Ja               |


**Datenbank-Unterstützung**  
Der Hauptvorteil von PDO gegenüber MySQLi liegt in der Unterstützung der Datenbanktreiber. Zum heutigen Zeitpunkt unterstützt PDO 12 verschiedene Treiber im Gegensatz zu MySQLi, das nur MySQL unterstützt.  


**API-Unterstützung**  
Sowohl PDO als auch MySQLi bieten eine objektorientierte API, aber MySQLi bietet auch eine prozedurale API - was das Verständnis für Neueinsteiger einfacher macht. Wenn Sie mit dem nativen PHP-MySQL-Treiber vertraut sind, werden Sie die Migration auf die prozedurale MySQLi-Schnittstelle viel einfacher finden. Sobald Sie PDO beherrschen, können Sie es mit jeder beliebigen Datenbank verwenden!  


**Connection**  
```php
// PDO
$pdo = new PDO("mysql:host=localhost;dbname=database", 'username', 'password');
 
// mysqli, procedural way
$mysqli = mysqli_connect('localhost','username','password','database');
 
// mysqli, object oriented way
$mysqli = new mysqli('localhost','username','password','database');
```

**Vorbereitete Anweisungen (Prepared Statements)**  
Eine vorbereitete Anweisung ist eine Funktion, mit der gleiche (oder ähnliche) SQL-Anweisungen wiederholt und mit hoher Effizienz ausgeführt werden können.  


**Objekt-Mapping**  
Sowohl PDO als auch MySQLi können Ergebnisse auf Objekte abbilden. Dies ist nützlich, wenn Sie keine benutzerdefinierte Datenbank-Abstraktionsschicht verwenden möchten, aber trotzdem ORM-ähnliches Verhalten wünschen. Stellen Sie sich vor, Sie haben eine Benutzerklasse mit einigen Eigenschaften, die mit Feldnamen aus einer Datenbank übereinstimmen.  

(ORM = Object-Relational Mapping)  

___
box/20.06.2018