<?php

$host = '';
$user = '';
$pass = '';
$db = '';

try {

   $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);

   if($conn) {
    echo "Connected to the <strong>$db</strong> database successfully";    
   }
}

catch(PDOExeption $e) {
   
   echo "Connection failed: " . $e->getMessage();

}

?>